<?php

/**
 * Process variables for page.tpl.php.
 */
function diary_preprocess_page(&$vars) {
  $node = menu_get_object();
  
  // Make the node creation timestamp available on pages that display a single
  // full node.
  $vars['node_created'] = '';
  if (!empty($node) && variable_get('node_submitted_' . $node->type, TRUE)) {
    $vars['node_created'] = $node->created;
  }
}

/**
 * Process variables for html.tpl.php.
 */
function diary_process_html(&$vars) {
  $node = menu_get_object();
  
  // Add an extra class on pages that display a single full node.
  if (!empty($node)) {
    $vars['classes'] .= ' page-node-full';
  }
}
