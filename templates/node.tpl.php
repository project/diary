<?php
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php if (!$page && $contextual_links): ?>
    <?php print render($contextual_links); ?>
  <?php endif; ?>

  <?php if ($display_submitted && !$page): ?>
    <div class="date">
      <?php print format_date($created, 'custom', 'M Y'); ?>
      <div class="day"><?php print format_date($created, 'custom', 'j'); ?></div>
    </div>
  <?php endif; ?>
  
  <?php if (!empty($content['links']['terms'])): ?>
    <div class="terms terms-inline"><?php print render($content['links']['terms']); ?></div>
  <?php endif; ?>
  
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $node_title; ?></a></h2>
  <?php endif; ?>

  <?php if ($display_submitted): ?>
    <div class="author">
      <?php print t('Submitted by !username', array('!username' => $name)); ?>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
  </div>

  <?php if (!empty($content['links'])): ?>
    <div class="links">
      <?php print render($content['links']); ?>
    </div>
  <?php endif; ?>
  
  <?php print render($content['comments']); ?>

</div>
