<?php
?>
<div id="page-wrapper">
  <div id="page">

    <div id="header" class="clearfix">
      <?php if ($logo || $site_name): ?>
        <div id="logo-and-site-name">
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
            <?php if ($logo): ?><img src="<?php print $logo; ?>" alt="" id="logo" /><?php endif; ?>
            <?php print $site_name; ?>
          </a>
        </div>
      <?php endif; ?>

      <?php if ($site_slogan): ?>
        <div id="site-slogan"><?php print $site_slogan; ?></div>
      <?php endif; ?>
    </div>

    <div id="main-wrapper">
      <div id="main-content" class="clearfix">

        <div id="content">
          <?php print $messages; ?>
    
          <?php if (!empty($node_created)): ?>
            <div class="date">
              <?php print format_date($node_created, 'custom', 'M Y'); ?>
              <div class="day"><?php print format_date($node_created, 'custom', 'j'); ?></div>
            </div>
          <?php endif; ?>
    
          <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
          <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
          <?php print render($page['help']); ?>
          <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
          <?php print render($page['content']); ?>
          <?php print $feed_icons; ?>
        </div>

        <?php if (!empty($main_menu) || $page['sidebar_first']): ?>
          <div id="sidebar">
            <?php print theme('links', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links', 'clearfix')))); ?>
            <?php print render($page['sidebar_first']); ?>
          </div>
        <?php endif; ?>  
      
        <span class="corner corner-left-top"></span>
        <span class="corner corner-right-top"></span>
        <span class="corner corner-right-bottom"></span>
        <span class="corner corner-left-bottom"></span>

      </div>
    </div>

    <div id="footer" class="clearfix">
      <?php print theme('links', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu', 'class' => array('links')))); ?>
      <?php print render($page['footer']); ?>
    </div>

  </div>
  
  <span class="bg"></span>
  
</div>
